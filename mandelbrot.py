# https://www.ibm.com/developerworks/community/blogs/jfp/entry/How_To_Compute_Mandelbrodt_Set_Quickly?lang=en

import numpy as np
from numba import jit
import matplotlib.pyplot as plt
import pygame


@jit
def mandelbrot(c,maxiter):
    z = c
    for n in range(maxiter):
        if abs(z) > 2:
            return n
        z = z*z + c
    return 0


@jit
def mandelbrot_set(xmin,xmax,ymin,ymax,width,height,maxiter):
    r1 = np.linspace(xmin, xmax, width)
    r2 = np.linspace(ymin, ymax, height)
    n3 = np.empty((width,height))
    for i in range(width):
        for j in range(height):
            n3[i,j] = mandelbrot(r1[i] + 1j*r2[j],maxiter)
    return (r1, r2, n3)
    

def zoom_in(x_min, x_max, y_min, y_max, amount=0.90):
    result = []
    for coords in [[x_min, x_max], [y_min, y_max]]:
        c_min, c_max = coords
        c_diff = c_max - c_min # Get difference
        c_mid = c_min + 0.5 * c_diff # Find midpoint
        c_min_sym = c_min - c_mid # Center around midpoint
        c_max_sym = c_max - c_mid # Center around midpoint
        c_min_sym = c_min_sym * 0.9 # Scale around midpoint
        c_max_sym = c_max_sym * 0.9 # Scale around midpoint
        c_min = c_min_sym + c_mid # Retranslate
        c_max = c_max_sym + c_mid  # Retranslate
        result.extend([c_min, c_max])
    return result


width = 600
height = 600
screen = pygame.display.set_mode((width, height))
pygame.display.flip()

x_min = -1
x_max = 1
y_min = -1
y_max = 1

r1, r2, n3 = mandelbrot_set(x_min, x_max, y_min, y_max, width, height, 200)

running = True

while running:
    # Poll for 'quit' event (so we can shutdown)
    event = pygame.event.poll()
    if event.type == pygame.QUIT:
        running = False
    elif event.type == pygame.KEYDOWN:
        x_diff = x_max - x_min
        y_diff = y_max - y_min
        if event.key == pygame.K_RIGHT:
            x_min += x_diff / 10
            x_max += x_diff / 10
        elif event.key == pygame.K_LEFT:
            x_min -= x_diff / 10
            x_max -= x_diff / 10
        elif event.key == pygame.K_DOWN:
            y_min += y_diff / 10
            y_max += y_diff / 10
        elif event.key == pygame.K_UP:
            y_min -= y_diff / 10
            y_max -= y_diff / 10
        elif event.key == pygame.K_SPACE:
            x_min, x_max, y_min, y_max = zoom_in(x_min, x_max, y_min, y_max)

        r1, r2, n3 = mandelbrot_set(x_min, x_max, y_min, y_max, width, height, 200)

    surf = pygame.surfarray.make_surface(n3)
    screen.blit(surf, (0, 0))
    pygame.time.delay(40)
    pygame.display.flip()