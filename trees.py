import argparse

import pygame
import numpy as np 
from collections import namedtuple

# Create a Point named tuple for storing point coords
Point = namedtuple('Point', ['x', 'y'])


def parse_args():
    parser = argparse.ArgumentParser(description='Make pretty fractal trees')
    parser.add_argument('--animate', help='Make interactive animation? If set to false, the program returns a still frame and (optional) image file', default=False, type=bool)
    parser.add_argument('--left_branch_angle', help='Maximum angle away from centre trunk for left branches', default=45, type=int)
    parser.add_argument('--right_branch_angle', help='Maximum angle away from centre trunk for right branches', default=45, type=int)
    parser.add_argument('--trunk_ratio', help='How long to keep trunk before branching', default=0.5, type=float)
    parser.add_argument('--iteration', help='How many iterations to draw', default=12, type=int)
    return parser.parse_args()


class Parameters:
    def __init__(self, width, height, args):
        self.left_branch_angle = np.math.radians(np.random.randint(0, args.left_branch_angle))
        self.right_branch_angle = np.math.radians(np.random.randint(0, args.left_branch_angle))
        self.trunk_ratio = args.trunk_ratio or np.clip(np.random.rand(), 0.1, 0.9)
        self.p0 = Point(width / 2, height - 0.2 * height)
        self.p1 = Point(width / 2, height - 0.8 * height)
        self.bgcolor = (0, 0, 0)
        self.fgcolor = (255,255,255)



def tree(p0, p1, limit, params):
    # Calculate x and y distance between p0 and p1
    dx = p1.x - p0.x
    dy = p1.y - p0.y
    dist = np.math.sqrt(dx ** 2 + dy ** 2)

    # Get angle 
    angle = np.math.atan2(dy, dx)
    branch_length = dist * (1 - params.trunk_ratio)

    # Create branching point (pA) and new branch endings (pB pC)
    pA = Point(p0.x + dx * params.trunk_ratio, p0.y + dy * params.trunk_ratio)

    # 'Rotate' branches by transforming end points based on angle and length
    pB = Point(
        x=pA.x + np.math.cos(angle + params.left_branch_angle) * branch_length,
        y=pA.y + np.math.sin(angle + params.left_branch_angle) * branch_length
        )
    # For right branch, we subtract angle instead of adding
    pC = Point(
        x=pA.x + np.math.cos(angle - params.right_branch_angle) * branch_length,
        y=pA.y + np.math.sin(angle - params.right_branch_angle) * branch_length
        )

    # Draw trunk
    pygame.draw.line(screen, params.fgcolor, p0, pA)

    # If we haven't reached the limit, recursively compute branches
    if limit > 0:
        tree(pA, pB, limit - 1, params)
        tree(pA, pC, limit - 1, params)
    else:
        # Otherwise draw the final branches
        pygame.draw.line(screen, params.fgcolor, pA, pB)
        pygame.draw.line(screen, params.fgcolor, pA, pC)



if __name__ == "__main__":

    # Create window for visualisation
    width, height = (1920, 1080)
    screen = pygame.display.set_mode((0,0), pygame.FULLSCREEN)
    pygame.display.flip()

    # Parse cli arguments
    args = parse_args()
    # Randomise params
    params = Parameters(width, height, args)

    # Iteration counter
    iters = 0
    # Draw tree until iteration limit, then keep drawing new random trees
    running = True
    # Animation stuff FIXME: This is shit, make it better
    angle_noise = np.random.choice((0.01, -0.01))
    while running:
        if args.animate:
            params.left_branch_angle += angle_noise
            params.right_branch_angle += angle_noise

        # Poll for 'quit' event (so we can shutdown)
        event = pygame.event.poll()
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                params = Parameters(width, height, args)
            elif event.key == pygame.K_RETURN:
                pygame.image.save(screen, "screenshot.bmp")

        # Keep iterating if we haven't reached limit
        if iters < args.iteration and args.animate:
            screen.fill(params.bgcolor)
            tree(params.p0, params.p1, round(iters), params)
        
        # Otherwise reinitialise parameters and reset iteration counter
        else:
            screen.fill(params.bgcolor)
            tree(params.p0, params.p1, round(args.iteration), params)

        # Increment counter
        iters = iters + 0.1 # Incrementing in fractions to allow for slower animation of trees
        # Add some delay for visualisation purposes
        pygame.time.delay(20)
        # Flush the display
        pygame.display.flip()